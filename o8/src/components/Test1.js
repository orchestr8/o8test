/*
For this test, you will notice that <Toggle> is the parent of few components:
- `<ToggleOn />` renders children when the `on` state is `true`
- `<ToggleOff />` renders children when the `on` state is `false`
- `<ToggleButton />` renders the `<Switch />` with the `on` prop set to the `on`
  state and the `onClick` prop set to `toggle`.

Based on that we have a Toggle component that manages the state, and we want to render
different parts of the UI however we want. We want control over the presentation
of the UI.

Final result should show "The button is on" when toggle is clicked and "The button is off" when it is not.

Test1.js is the only file you should modify. 
!!! You do not need to add any additional state. !!!
*/

import * as React from "react";
import { Switch } from "./switch";

const Toggle = () => {
  const [on, setOn] = React.useState(false);
  const toggle = () => setOn(!on);

  return <Switch on={on} onClick={toggle} />;
};

const ToggleOn = () => null;

const ToggleOff = () => null;

const ToggleButton = () => null;

const Test1 = () => {
  return (
    <div>
      <Toggle>
        <ToggleOn>The button is on</ToggleOn>
        <ToggleOff>The button is off</ToggleOff>
        <ToggleButton />
      </Toggle>
    </div>
  );
};

export default Test1;
