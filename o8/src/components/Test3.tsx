/*
This test is about showing a confirmation message to the user.
Please note that for this exercice we are using a tsx file. 

There is nothing yet in this file so you will have to build it from scratch.

In order to display a message you will use the snackbar from material-ui, you can either install the framework or use the UMD.

The page will consist of 1 input field and 1 button.
When the user clicks on the button, you will have to check if the value of the input contains the string 'o8' and if so a success snackbar will appear on the TOP LEFT of the navigator.
if the string does not contain 'o8' a warning snackbar will appear on the BOTTOM CENTER of the page.
In case the user clicks on the button when the input is still empty. Show an error snackbar on the BOTTOM CENTER of the page.

Please apply to following style to the button:
height: 42px;
width: 209px;
left: 0px;
top: 0px;
border-radius: 4px;
padding: 8px, 22px, 8px, 22px;

----------------------------------
QUESTION BEFORE STARTING TO CODE:- 
----------------------------------
Do you have a better solution to propose instead of show an error snackbar when the field is empty?

ANSWER HERE
XXXXXXXXXXX

*/
