/*
    Sharing state between components is often a problem.
    We usually solve this problem by "lifting the state". However, this solution requires [prop drilling] which is not a problem but sometimes prop drilling can become a real pain.

    To avoid this pain, we can add some state into a section of the react tree, and then reuse this state anywhere in our react tree without having to pass it everywhere explicitly.
    The purpose of this test is to avoid this pain :) 

    For the sake of this test, we will put everything in this file.

    Your job here is to create CountProvider function component and use this reusable state pattern.

    Final result of this test: Each time we click on "Add 1 in count", we increment the count var and display it via CountDisplay
*/

import * as React from "react";

// Here you should create this variable which will normaly set in a separated file

//
const CountProvider = (props) => {};

const CountDisplay = () => {
  // get the count from the 'Global' variable you created"
  const count = 0;
  return <div>{`The current count is ${count}`}</div>;
};

const Counter = () => {
  // get the setCount from the 'Global' var you created"
  const setCount = () => {};
  const increment = () => setCount((c) => c + 1);
  return <button onClick={increment}>Add 1 in count</button>;
};

function App() {
  return (
    <div>
      <CountProvider>
        <CountDisplay />
        <Counter />
      </CountProvider>
    </div>
  );
}

export default App;
